const path = require('path');
const webpack = require('webpack');
const {merge} = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const baseConfig = require('./webpack.base.conf');
// 应用名称、版本号
const {name, version} = require("../package.json");

module.exports = merge(baseConfig, {
    mode: 'development',
    entry:{
        [name]: path.join(__dirname,'../example/src/index.ts'),
        // [`${name}.min`]: path.join(__dirname,'../src/index.ts'),
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'calendar',
            favicon: path.join(__dirname, '../example/favicon.ico'),
            template: path.join(__dirname, '../example/index.html'),
            inject: true
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ],
    devServer:{
        host: 'localhost',
        port: 6688,
        hot: true, // 默认true 热更新，与HotModuleReplacementPlugin插件搭配使用
        open: true, // 自动打开浏览器
        proxy: {
            '/api':{
                target: 'http://127.0.0.1:3000', // 代理地址
                changeOrigin: true,//改变源
                pathRewrite:{
                    '^/api':'',//路径重写
                }
            }
        },
    },
    stats: 'errors-warnings', // 只在发生错误或有警告时输出
})
