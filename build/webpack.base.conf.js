const path = require('path');
// 应用名称、版本号
const {name, version} = require("../package.json");
const publicPath = process.env.NODE_ENV === 'production'?'./':'/';
const ouPutPath = path.join(__dirname, '../lib/');
module.exports = {
    mode: 'production',
    entry:{
        [name]: path.join(__dirname,'../src/index.ts'),
        // [`${name}.min`]: path.join(__dirname,'../src/index.ts'),
    },
    output: {
        path: ouPutPath, // 默认 dist
        filename:`[name].js`,// filename:`assets/js/[name].[hash:8].js`,
        // chunkFilename:`[name].[hash:8].js`, //webpackChunkName
        publicPath: publicPath

        // filename:"[name].js",
        // library: [name],
        // libraryExport: "default",
        // libraryTarget: "umd",
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    'style-loader',
                    {
                        loader: "css-loader"
                    },
                    {
                        loader:'sass-loader'
                    },
                    {
                        loader: "postcss-loader",
                    }
                ],
            },
            {
                test: /\.(jpg|png|gif|svg|webp|jpeg)$/,
                use: [
                    {
                        loader:"file-loader",
                        options:{
                            name:"[name].[ext]",
                            publicPath:'/images/',
                            outputPath:'images/'
                        }
                    },
                ]
            },
            // {
            //     test: /\.(html|ejs)$/,
            //     use: ['html-loader']
            // }
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ],
        // 别名，使用时在别名前面加一个'~'，告诉加载器它是一个模块，而不是相对路径
        alias:{
            '@': path.resolve(__dirname,'../src')
        }
    },
}
