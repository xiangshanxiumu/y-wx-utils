const path = require('path');
const {merge} = require('webpack-merge');
const TerserPlugin = require("terser-webpack-plugin");
//删除压缩包，删除输出位置生成的文件
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
//提取css到单独文件的插件
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
//引入压缩css的插件
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// 文件复制移动
const CopyWebpackPlugin = require("copy-webpack-plugin");
// 应用名称、版本号
const {name, version} = require("../package.json");
const baseConfig = require('./webpack.base.conf');

module.exports = merge(baseConfig, {
    mode: 'production',
    entry:{
        [name]: path.join(__dirname,'../src/index.ts'),
        // [`${name}.min`]: path.join(__dirname,'../src/index.ts'),
    },
    output: {
        filename:"[name].js",
        library: [name],
        libraryExport: "default",
        libraryTarget: "umd",
    },
    module:{
        rules:[
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: "sass-loader"
                    },
                    {
                        loader: "postcss-loader",
                    }
                ],
            },
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ]
    },
    optimization: {
        minimize: true,
        minimizer: [
            //压缩css
            new OptimizeCssAssetsPlugin(),
            new TerserPlugin({
                include:/\.min\.js$/
            })
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'style/[name].css',//都提到build目录下的css目录中
            chunkFilename: 'style/[id].css',
            // ignoreOrder: false, // Enable to remove warnings about conflicting order
        }),
        //压缩css
        new OptimizeCssAssetsPlugin(),
        // clear dist
        new CleanWebpackPlugin(),
        // new CheckerPlugin(),
        // new CopyWebpackPlugin({
        //     patterns: [
        //         { from: path.resolve(__dirname, '../src/types/'), to: path.resolve(__dirname, '../lib/types') },
        //     ],
        // }),
    ],
});
