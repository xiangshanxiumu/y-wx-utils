# y-wx-utils


### 介绍
> 微信h5公众号、小程序开发使用的相关工具方法库

### 安装教程
```bash
  npm i y-wx-utils -S
```

### 使用教程

- 全局引入使用
```ts
// vue应用main.ts 或 taro 小程序工程 app.ts 入口文件引入注册
import Vue from 'vue';
import yWxUtils from 'y-wx-utils';
Vue.prototype.$wxUtils = yWxUtils;

// 页面组件中使用
this.$wxUtils.xxx
```
----------------------------------------------------------
- 按需局部引入使用
```ts
// 或按需引入
import {common, wxh5, wxMiniProgram} from 'y-wx-utils';

// 获取数据类型
const dataType = common.getDataType();
// 检测是否微信浏览器环境
const isWxBrowser = wxh5.checkWxBrowser();

```
------------------------------------------------------------
- 单独引入某个函数方法
```ts
import {getDataType, checkWxBrowser} from "y-wx-utils";

// 获取数据类型
const dataType = getDataType();
// 检测是否微信浏览器环境
const isWxBrowser = checkWxBrowser();
```
### 工具方法介绍
| 工具方法 | 作用| 模块 |
|:-----|:-----|:-----|
|getDataType|获取数据类型| common |
|deepClone |深度克隆数据对象| common |
|convertCodeURIParams|微信小程序url传参,url参数转换| common |
|filterHtmlStringByWhite|输入框、富文本等输入的特殊字符，如标签转义过滤方法| common |
|checkWxBrowser|检测是否微信浏览器环境| wxh5 |
|getPhoneType|获取手机操作系统型号| wxh5 |
|getUrlQueryParam|url中query参数转换获取| wxh5 |
|wxOaLogin|微信H5公众号登录方法| wxh5 |
|getWxCode|微信H5公众号发起登录后获取code| wxh5 |
|backToWxOaMenu|微信公众号H5手动回退到公众号菜单窗口| wxh5 |
|disabledWxOaBackHistoryRoute|禁用历史路由回退| wxh5 |
|getTaroMiniProgramElementRect|taro小程序获取元素相关位置信息| wxMiniProgram |
