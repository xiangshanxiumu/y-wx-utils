/* 微信h5、小程序开发公共工具函数方法 */
import {Common} from "../../types";

/**
 * @method getDataType
 * @description 获取数据类型
 * @param data: any
 * @return string;
 * */
export const getDataType = (data: any) => {
    return Object.prototype.toString.call(data).slice(8, -1);
}
/**
 * @method deepClone
 * @description 深度克隆数据对象
 * @param data
 * @return newData
 * */
export const deepClone = (data: any) => {
    let str, newData: any = data.constructor === Array ? [] : {};
    if(typeof data !== 'object') {
        return;
    } else if(data instanceof Array){
        for(const i in data) {
            newData[i] = typeof data[i] === 'object' ? deepClone(data[i]) : data[i];
        }
    } else {
        str = JSON.stringify(data);
        newData = JSON.parse(str);
    }
    return newData;
}
/**
 * @method convertCodeURIParams
 * @description 微信小程序url传参，参数转换，否则参数易丢失
 * @param params 参数对象
 * @param type 转换类型【encode/decode】,默认 encode
 * @return params
 * */
export const convertCodeURIParams = (params: any, type: string = 'encode') => {
    // convertParams
    function convertParams(params: any, index: string | number) {
        const isObjectOrArray = ('Object' === getDataType(params[index])) || ('Array' === getDataType(params[index]));
        if(isObjectOrArray) {
            codeURI(params[index]);
        } else {
            encodeOrDecode(params[index]);
        }
    }
    // encodeURIComponent decodeURIComponent
    function encodeOrDecode(param: any){
        if(type === 'encode') {
            // encode 处理 Undefined\Null\Number类型转换为String
            param = encodeURIComponent(param);
        } else {
            // decode
            param = decodeURIComponent(param);
            // undefined
            if ('undefined' === param){
                param = undefined;
            }
            // null
            if ('null' === param){
                param = null;
            }
            // number 不处理
        }
    }
    // codeURI
    function codeURI(params: any) {
        if('Object' === getDataType(params)) {
            for(let key in params) {
                convertParams(params, key);
            }
        } else if('Array' === getDataType(params)) {
            for(let i = 0; i< params.length; i++) {
                convertParams(params, i);
            }
        } else {
            encodeOrDecode(params);
        }
        return params;
    }

    return codeURI(params);
}

/**
 * @method filterHtmlStringByWhite
 * @description 富文本等 特殊字符标签转义过滤方法
 * @param htmlString
 * @param whiteList
 * @return params
 * */
export const filterHtmlStringByWhite = (htmlString: string, whiteList: string[]) => {
    if('String' !== getDataType(htmlString)) return htmlString;
    // 白名单列表
    if(!whiteList) whiteList = ['p','br','img','a'];
    // 计算获取 ['/p','/br','/img','/a']
    whiteList.map((item: any) => {
        return `/${item}`;
    });
    // 计算过滤正则
    const whiteRegExp = whiteList.join('|'); // p|br|img|a
    const white2RegExp = whiteList.join('|'); // p|br|img|a
    const filterRegExp = `<(?!${whiteRegExp}|${white2RegExp})[^>]+>`;
    const reg = new RegExp(filterRegExp, 'gi');
    return htmlString.replace(reg, '');
}

export const common: Common = {
    getDataType,
    deepClone,
    convertCodeURIParams,
    filterHtmlStringByWhite,
}
