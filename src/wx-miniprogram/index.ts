/* 微信小程序开发使用的相关工具方法 */
import {TaroMiniProgramElementRectOptions, WxMiniProgram} from "../../types/wx-miniprogram";

/**
 * @method getTaroElementRect
 * @description taro小程序 获取元素相关位置信息
 * @param options: TaroMiniProgramElementRectOptions
 * @return rect: {width,height,...}
 * */
export const getTaroMiniProgramElementRect = (options: TaroMiniProgramElementRectOptions) => {
    const element = options?.id || options?.className;
    const timeOut = options.timeOut || 100;
    const taroInstance = options?.taro;
    return new Promise((resolve, reject) => {
        const query = taroInstance.createSelectorQuery();
        setTimeout(() => {
            query.select(element).boundingClientRect((rect: any) => {
                resolve(rect);
            }).exec();
        }, timeOut);
    })
}

// 微信 小程序工具方法集
export const wxMiniProgram: WxMiniProgram = {
    getTaroMiniProgramElementRect,
}
