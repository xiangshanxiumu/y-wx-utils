/* 微信h5、小程序开发使用的相关工具方法 */
export * from './common/index';
// 微信h5相关工具函数方法集
export * from './wx-h5/index';
// 微信小程序相关工具函数方法集
export * from './wx-miniprogram/index';
