/* 微信小程序开发使用的相关工具方法 types */

// getTaroMiniProgramElementRect options
export type TaroMiniProgramElementRectOptions = {
    id?: string|undefined; // 元素id
    className?: string|undefined; // class 样式名
    timeOut?: number|undefined;
    taro: any; // taro 对象
}

export declare function getTaroMiniProgramElementRect(): any;

export declare type WxMiniProgram = {
    getTaroMiniProgramElementRect?:Function;
}
