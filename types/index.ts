/* 导出所以相关方法申明 */
export * from './common';
// 微信h5相关工具函数方法 申明
export * from './wx-h5/index';
// 微信小程序相关工具函数方法 申明
export * from './wx-miniprogram/index';
