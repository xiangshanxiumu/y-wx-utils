/* 微信h5开发使用的相关工具方法 types */
// 微信H5公众号登录方法 传参
export declare type WxOaLoginOptions = {
    appId: string;
    redirect_url: string;
    scope: string;
    state?: string;
}
// 检测是否微信浏览器环境
export declare function checkWxBrowser(): boolean;
// 获取手机操作系统类型
export declare function getPhoneType(): string;
// url中参数转换获取
export declare function getUrlQueryParam(): Object;
// 微信H5公众号登录方法
export declare function wxOaLogin(): void;
// 微信H5公众号发起登录后获取code
export declare function getWxCode(): string;

// 微信公众号手动回退到公众号菜单窗口
export declare function backToWxOaMenu(): void;
// 监听路由历史回退，回到公众号菜单窗口
export declare function watchHistoryBackWxOaMenu(): void;
// 禁止微信公众号H5回退历史路由
export declare function disabledWxOaBackHistoryRoute(): void;


export declare type WxH5 = {
    checkWxBrowser ?:Function;
    getPhoneType ?:Function;
    getUrlQueryParam ?:Function;
    wxOaLogin ?:Function;
    getWxCode ?:Function;
    backToWxOaMenu ?:Function;
    watchHistoryBackWxOaMenu ?:Function;
    disabledWxOaBackHistoryRoute ?:Function;
}
