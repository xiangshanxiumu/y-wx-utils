/* 微信h5、小程序开发公共工具函数方法 types */

// 获取数据类型
export declare  function getDataType(): string;
// url传参参数转换
export declare  function convertCodeURIParams(): any;
// 特殊标签字符过滤转换 【防止输入脚本、sql注入等攻击】
export declare  function filterHtmlStringByWhite(): string;
// 深度克隆数据
export declare function deepClone(): any;

export declare type Common = {
    getDataType ?:Function;
    convertCodeURIParams ?:Function;
    filterHtmlStringByWhite ?:Function;
    deepClone ?:Function;
}
